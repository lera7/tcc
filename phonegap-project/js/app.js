document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
	console.log('onDeviceReady');
	//navigator.notification.alert('Hello World!', null, 'Hello!','Thanks!');
	getContacts();
}


function getContacts() {
	console.log('getContacts');
	var fields = ['name', 'displayName', 'phoneNumbers'];
	var options = new ContactFindOptions();
	options.filter = "";
	options.multiple = true;
	navigator.contacts.find(fields, onGetContactsSuccess, onGetContactsError, options);
	$('#contact_list').empty();
}

function onGetContactsSuccess(contacts) {
	console.log('onGetAllContactsSuccess', contacts);
	console.log('jquery', $);
	console.log('total contacts: ' + contacts.length);

	if (contacts.length>0) {
		contacts.sort(sortAlphabetical);
		for (var i=0; i<contacts.length; i++) {
			try {
				var contact = contacts[i];
	            		if (contact.phoneNumbers[0].value.length > 7) {
					console.log('contact', contact);
					strContactTemplate = '<li>';
					if (contact.photos) {
						strContactTemplate += '<img src="'+contact.photos[0].value+'">';
					}
					strContactTemplate += '<h2>'+contact.name+'</h2>';
					strContactTemplate += '<p>'+contact.phoneNumbers[0].value+'</p>';
					strContactTemplate += '</li>';
					console.log('contact html', strContactTemplate);
					$('#contact_list').append(strContactTemplate);
	            		}
			}catch(error) {
				console.log('error', error);
			}
		}
		try {
			$('#contacts_list').listview('refresh');
		}catch(error) {
			console.log('refresh list', error);
		}
	} else {
		$('#contact_list').append('<li><h3>Nenhum contato encontrado</h3></li>');
	}
}

function onGetContactsError() {
	console.log('onGetAllContactsError');
	console.log(arguments);
}


function sortAlphabetical(a, b) {
	if (a.name.formatted < b.name.formatted){
		return -1;
	}else if (a.name.formatted > b.name.formatted){
		return  1;
	}else{
		return 0;
	}
}
